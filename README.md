# Spletni barvni stroboskop

1\. domača naloga pri predmetu [Osnove informacijskih sistemov](https://ucilnica.fri.uni-lj.si/course/view.php?id=54) (navodila)


## 6.1 Opis naloge in navodila

Na Bitbucket je na voljo javni repozitorij [https://bitbucket.org/asistent/stroboskop](https://bitbucket.org/asistent/stroboskop), ki vsebuje nedelujoč spletni stroboskop. V okviru domače naloge ustvarite kopijo repozitorija ter popravite in dopolnite obstoječo implementacijo spletne strani tako, da bo končna aplikacija z vsemi delujočimi funkcionalnostimi izgledala kot na spodnji sliki. Natančna navodila si preberite na [spletni učilnici](https://ucilnica.fri.uni-lj.si/mod/workshop/view.php?id=19181).

![Stroboskop](stroboskop.gif)

## 6.2 Vzpostavitev repozitorija

Naloga 6.2.2:

```
git clone git@bitbucket.org:tilenbabnik/stroboskop.git
```

Naloga 6.2.3:
https://bitbucket.org/tilenbabnik/stroboskop/commits/b3b5a4cdaa9b77bff7782e45c418f2dd7f9e90d5

## 6.3 Skladnja strani in stilska preobrazba

Naloga 6.3.1:
https://bitbucket.org/tilenbabnik/stroboskop/commits/18480138fadb698252e7fc6183a4b262faf49157

Naloga 6.3.2:
https://bitbucket.org/tilenbabnik/stroboskop/commits/59d1a9ea185c4f1fee4c318aff16e6b5798219ef

Naloga 6.3.3:
https://bitbucket.org/tilenbabnik/stroboskop/commits/2c709f8fd1d6b51b6a3eea370613046b91aa7af6

Naloga 6.3.4:
https://bitbucket.org/tilenbabnik/stroboskop/commits/25ce9fcef4fcce63d5695c6fe3b77f6d50381fcd

Naloga 6.3.5:

```
git checkout master
git merge izgled
git pull
git push --all
```

## 6.4 Dinamika in animacija na strani

Naloga 6.4.1:
https://bitbucket.org/tilenbabnik/stroboskop/commits/fcf82b5f590ca67599b19680a8560d9d97b09311?at=dinamika

Naloga 6.4.2:
https://bitbucket.org/tilenbabnik/stroboskop/commits/b11a08848556e928f246b41a6fdf649aa5a5a6e0?at=dinamika

Naloga 6.4.3:
https://bitbucket.org/tilenbabnik/stroboskop/commits/09de44655748eee411466c3f3e824f3d7661a2c2?at=dinamika

Naloga 6.4.4:
https://bitbucket.org/tilenbabnik/stroboskop/commits/0c7a15d8b4efa6e0796c123e99fc26efc7ed86e8